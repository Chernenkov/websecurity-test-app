<?php
// define(MODE, DEVELOPMENT); // DEVELOPMENT
define(MODE, PRODUCTION); // PRODUCTION

if(MODE === DEVELOPMENT) {
  define(PRINT_DEBUG, true);
  define(INI_DISPLAY_ERRORS, 1);
  define(INI_DISPLAY_STARTUP_ERRORS, 1);
  define(ERROR_REPORTING, E_ALL);
}else {
  define(PRINT_DEBUG, false);
  define(INI_DISPLAY_ERRORS, 0);
  define(INI_DISPLAY_STARTUP_ERRORS, 0);
  define(ERROR_REPORTING, E_ERROR);
}
 //"" ,E_ERROR | E_WARNING | E_PARSE | E_NOTICE
ini_set('display_errors', INI_DISPLAY_ERRORS);
ini_set('display_startup_errors', INI_DISPLAY_STARTUP_ERRORS);

require_once '/usr/share/nginx/twig/vendor/autoload.php';
require '/usr/share/nginx/php/functions.php';

$loader = new Twig_Loader_Filesystem('/usr/share/nginx/twig/templates');
$twig = new Twig_Environment($loader);

if($_SERVER['REQUEST_METHOD'] === 'GET') {
  $page = $_GET['page'];
  switch($page) {
        case 1:
              // draw
              $template = $twig->loadTemplate('list.html');
              echo $template->render(array());
              // get table name from GET
              $table_name = $_GET['table_name'];
              // connect
              $PDO = connect();
              // draw table
              if($table_name != "") {
                  draw($table_name, $twig, $PDO); // choose which table to draw due to get parameter
              }
              printDebugMsg("Page = list");
              break;
        case 2:
              $template = $twig->loadTemplate('add.html');
              echo $template->render(array());
              printDebugMsg("Page = add");
              break;
        case 3:
              $PDO = connect();
              $template = $twig->loadTemplate('change.html');
              $incidents = $PDO->query("SELECT id_global_incident, name, authored_by, date FROM Incident;");
              echo $template->render(array(
                'incidents' => $incidents
              ));
              $change = $_GET['change'];
              drawChangeForm($change, $twig, $PDO);
              printDebugMsg("Page = change");
              break;
        case 4:
              $PDO = connect();
              $template = $twig->loadTemplate('delete.html');
              $incidents = $PDO->query("SELECT id_global_incident, name, authored_by, date FROM Incident;");
              echo $template->render(array(
                'incidents' => $incidents
              ));
              printDebugMsg("Page = delete");
              break;
        default:
              $template = $twig->loadTemplate('index.html');
              echo $template->render(array());
              printDebugMsg("Page = default");
              break;
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $operationType = $_POST['info'];
    switch ($operationType) {
      case 'delete':
        printDebugMsg("DEL attempt...");
        deleteLogic();
        break;
      case 'add':
        printDebugMsg("ADD attempt...");
        addLogic();
        break;
      case 'change':
        printDebugMsg("CHANGE attempt...");
        changeLogic();
        break;
      default:
        printDebugMsg("Wrong POST request attempt...");
        break;
    }
}

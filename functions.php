<?php

function connect() {
      try {
              $cs = "mysql:host=localhost;dbname=IncidentManagement";
              $dbuser = "analyst";
              $passwd = "1a2b3c";
              $PDO = new PDO($cs, $dbuser, $passwd);
              return $PDO;
      } catch(PDOException $e) {
            printDebugMsg($e->getMessage());
            error_log("[App] Failed while connecting with PDO on functions.php", 3, "/usr/share/nginx/applogs/app_events.log");
      }
}

function draw($getparam, $twig, $PDO) {
      switch($getparam) {
			case "categories":
        // echo "categories case running";
        $loader = new Twig_Loader_Filesystem('/usr/share/nginx/twig/templates');
        $twig = new Twig_Environment($loader);
        $template = $twig->loadTemplate('categories.html');
        $categories = $PDO->query("SELECT * FROM Categories;");
        echo $template->render(array(
  				'categories' => $categories
  			));
				break;
			case "sources":
        // echo "sources case running";
        $template = $twig->loadTemplate('sources.html');
				$sources = $PDO->query("SELECT * FROM IncidentSource;");
        echo $template->render(array(
  				'sources' => $sources
  			));
        break;
			case "honeyevents":
        // echo "honeyevents case running";
        $template = $twig->loadTemplate('honeyevents.html');
				$honeyevents = $PDO->query("SELECT * FROM HoneypotEvents;");
        echo $template->render(array(
  				'honeyevents' => $honeyevents
  			));
        break;
			case "syslogevents":
        // echo "syslogevents case running";
        $template = $twig->loadTemplate('syslogevents.html');
				$syslogevents = $PDO->query("SELECT * FROM SyslogEvents;");
        echo $template->render(array(
  				'syslogevents' => $syslogevents
  			));
        break;
			case "netsniffevents":
        // echo "netsniffevents case running";
        $template = $twig->loadTemplate('netsniffevents.html');
			  $netsniffevents = $PDO->query("SELECT * FROM NetsniffEvents;");
        echo $template->render(array(
      		'netsniffevents' => $netsniffevents
      	));
        break;
			default:
        // echo "Default case running";
        $template = $twig->loadTemplate('list.html');
				break;
			}
}

// function drawDeleteWindow(){
//       $loader = new Twig_Loader_Filesystem('/usr/share/nginx/twig/templates');
//       $twig = new Twig_Environment($loader);
//       $template = $twig->loadTemplate('delete.html');
//       $incidents = $PDO->query("SELECT id_global_incident, name, authored_by, date FROM Incident;");
//       echo $template->render(array(
//         'incidents' => $incidents
//       ));
// }

function drawChangeForm($getparamChange, $twig, $PDO){
      $incidentId = $getparamChange;
      $incidentIds = $PDO->query("SELECT id_global_incident FROM Incident;");
      if(isInIdRange($incidentId, $incidentIds)) {
            $template = $twig->loadTemplate('changeform.html');
            $fillData = $PDO->query("SELECT * FROM Incident WHERE id_global_incident=$incidentId");
            $dataArray = [];
            // echo $template->render(array());
            foreach ($fillData as $row) {
              $dataArray['id_global_incident'] = $row['id_global_incident'];
              $dataArray['name'] = $row['name'];
              $dataArray['category_id'] = $row['category_id'];
              $dataArray['date'] = $row['date'];
              $dataArray['authored_by'] = $row['authored_by'];
              $dataArray['nids_event_id'] = $row['nids_event_id'];
              $dataArray['hids_event_id'] = $row['hids_event_id'];
              $dataArray['honeypot_event_id'] = $row['honeypot_event_id'];
            }
            echo $template->render(array(
              'id_global_incident' => $dataArray['id_global_incident'],
              'name' => $dataArray['name'],
              'category_id' => $dataArray['category_id'],
              'date' => $dataArray['date'],
              'authored_by' => $dataArray['authored_by'],
              'nids_event_id' => $dataArray['nids_event_id'],
              'hids_event_id' => $dataArray['hids_event_id'],
              'honeypot_event_id' => $dataArray['honeypot_event_id']
            ));
      } else {
            printDebugMsg("No incident with such ID");
            error_log("[App] Attempt to change non-existing Incident", 3, "/usr/share/nginx/applogs/app_events.log");
      }
}

function drawAddForm() {
//
  $loader = new Twig_Loader_Filesystem('/usr/share/nginx/twig/templates');
  $twig = new Twig_Environment($loader);
  $template = $twig->loadTemplate('add.html');
  echo $template->render(array());
//
  $PDO = connect();
  // request IDs from table to check them
  $categoryIds = $PDO->query("SELECT id_category FROM Categories");
  $nidsIds = $PDO->query("SELECT id_nids_event FROM NIDSEvents");
  $hidsIds = $PDO->query("SELECT id_hids_event FROM HIDSEvents");
  $honeypotIds = $PDO->query("SELECT id_hp_event FROM HoneypotEvents");
  //
  // check variables
  $categoryIdOk = isInIdRange($_POST['category_id'], $categoryIds);
  if(!$categoryIdOk) {
    printDebugMsg("Invalid Category ID");
  }

  $nidsIdOk =  isInIdRange($_POST['nids_event_id'], $nidsIds);
  if(!$nidsIdOk) {
    printDebugMsg("Invalid NIDSEvent ID");
  }

  $hidsIdOk = isInIdRange($_POST['hids_event_id'], $hidsIds);
  if(!$hidsIdOk) {
    printDebugMsg("Invalid HIDSEvent ID");
  }

  $hpIdOk = isInIdRange($_POST['honeypot_event_id'], $honeypotIds);
  if(!$hpIdOk) {
    printDebugMsg("Invalid Noneypot Event ID");
  }
  $INPUT_VALID = $categoryIdOk
              && $nidsIdOk
              && $hidsIdOk
              && $hpIdOk;

  if($INPUT_VALID) {
    try{
          $data = [
            'name'              => $_POST['name'],
            'category_id'       => $_POST['category_id'],
            'date'              => $_POST['date'],
            'authored_by'       => $_POST['authored_by'],
            'nids_event_id'     => $_POST['nids_event_id'],
            'hids_event_id'     => $_POST['hids_event_id'],
            'honeypot_event_id' => $_POST['honeypot_event_id']
          ];
          $PDO = null;
          $PDO = connect();
          $query = "INSERT INTO Incident (name, category_id, date, authored_by,
                                          nids_event_id, hids_event_id, honeypot_event_id)
                    VALUES(:name, :category_id, :date, :authored_by, :nids_event_id, :hids_event_id, :honeypot_event_id);";
          $send = $PDO->prepare($query);
          $send->execute($data);
          echo "<h3>Incident Successfully Added!<h3>";
    } catch(PDOException $e) {
          $message = $e->getMessage(); // string
          printDebugMsg($message);
          error_log("[MySQL] INSERT query failed on add.php", 3, "/usr/share/nginx/applogs/app_events.log");
    }
    return true;
  } else{
        printDebugMsg("Invalid input data!");
        error_log("[App] Invalid input data while trying to add Incident", 3, "/usr/share/nginx/applogs/app_events.log");
        return false;
  }
}

function deleteLogic() {
    $PDO = null;
    $PDO = connect();
    $incidentIdToDelete = $_POST['ID'];
    printDebugMsg("ID of Incident to be deleted: $incidentIdToDelete");
    $IncidentIds = $PDO->query("SELECT id_global_incident FROM Incident");
    if(isInIdRange($incidentIdToDelete, $IncidentIds)) {
          printDebugMsg("Valid ID");
          try{
                $data = ['ID' => $incidentIdToDelete];
                $query = "DELETE from Incident WHERE id_global_incident=:ID";
                $send = $PDO->prepare($query);
                $send->execute($data);
          }catch(PDOException $e) {
                  printDebugMsg($e->getMessage());
                  error_log("[MySQL] DELETE query failed on delete.php", 3, "/usr/share/nginx/applogs/app_events.log");
          }
          echo "<br>Incident with ID ", $incidentIdToDelete, " deleted...";
    } else {
          echo "<br>There is no incident with such ID...";
          error_log("[App] Attempt to DELETE non-existing Incident", 3, "/usr/share/nginx/applogs/app_events.log");
    }
}

function addLogic(){
    $PDO = connect();
    // request IDs from table to check them
    $categoryIds = $PDO->query("SELECT id_category FROM Categories");
    $nidsIds = $PDO->query("SELECT id_nids_event FROM NIDSEvents");
    $hidsIds = $PDO->query("SELECT id_hids_event FROM HIDSEvents");
    $honeypotIds = $PDO->query("SELECT id_hp_event FROM HoneypotEvents");
    //
    // check variables
    $categoryIdOk = isInIdRange($_POST['category_id'], $categoryIds);
    if(!$categoryIdOk) {
      printDebugMsg("Invalid Category ID");
    }

    $nidsIdOk =  isInIdRange($_POST['nids_event_id'], $nidsIds);
    if(!$nidsIdOk) {
      printDebugMsg("Invalid NIDSEvent ID");
    }

    $hidsIdOk = isInIdRange($_POST['hids_event_id'], $hidsIds);
    if(!$hidsIdOk) {
      printDebugMsg("Invalid HIDSEvent ID");
    }

    $hpIdOk = isInIdRange($_POST['honeypot_event_id'], $honeypotIds);
    if(!$hpIdOk) {
      printDebugMsg("Invalid Noneypot Event ID");
    }
    $INPUT_VALID = $categoryIdOk
                && $nidsIdOk
                && $hidsIdOk
                && $hpIdOk;

    if($INPUT_VALID) {
      try{
            $data = [
              'name'              => $_POST['name'],
              'category_id'       => $_POST['category_id'],
              'date'              => $_POST['date'],
              'authored_by'       => $_POST['authored_by'],
              'nids_event_id'     => $_POST['nids_event_id'],
              'hids_event_id'     => $_POST['hids_event_id'],
              'honeypot_event_id' => $_POST['honeypot_event_id']
            ];
            $PDO = null;
            $PDO = connect();
            $query = "INSERT INTO Incident (name, category_id, date, authored_by,
                                            nids_event_id, hids_event_id, honeypot_event_id)
                      VALUES(:name, :category_id, :date, :authored_by, :nids_event_id, :hids_event_id, :honeypot_event_id);";
            $send = $PDO->prepare($query);
            $send->execute($data);
            echo "<h3>Incident Successfully Added!<h3>";
      } catch(PDOException $e) {
            $message = $e->getMessage(); // string
            printDebugMsg($message);
            error_log("[MySQL] INSERT query failed on add.php", 3, "/usr/share/nginx/applogs/app_events.log");
      }
    } else{
          echo "Invalid input data!";
          printDebugMsg("Invalid input data!");
          error_log("[App] Invalid input data while trying to add Incident", 3, "/usr/share/nginx/applogs/app_events.log");
    }
}

function changeLogic() {
    $incidentId = $_POST['id_global_incident'];
    $incidentName = $_POST['name'];
    $incidentCategoryId = $_POST['category_id'];
    $incidentDate = $_POST['date'];
    $incidentAuthor = $_POST['authored_by'];
    $incidentNidsEventId = $_POST['nids_event_id'];
    $incidentHidsEventId = $_POST['hids_event_id'];
    $incidentHoneyId = $_POST['honeypot_event_id'];

    $debugNewValues = "New values:<br> <br>ID: $incidentId <br>Name:  $incidentName
                      <br>Category ID:  $incidentCategoryId <br>Date:  $incidentDate
                      <br>Author: $incidentAuthor <br>NIDS Event ID:  $incidentNidsEventId
                      <br>HIDS Event ID: $incidentHidsEventId <br>Honeypot Event ID:  $incidentHoneyId";
    printDebugMsg($debugNewValues);

    try {

          $PDO = null;
          $PDO = connect();
          //validate
          // $incidentIds = $PDO->query("SELECT id_global_incident FROM Incident;"); // not changed anyway
          $categoryIds = $PDO->query("SELECT id_category FROM Categories");
          $nidsIds = $PDO->query("SELECT id_nids_event FROM NIDSEvents");
          $hidsIds = $PDO->query("SELECT id_hids_event FROM HIDSEvents");
          $honeypotIds = $PDO->query("SELECT id_hp_event FROM HoneypotEvents");
          if(isInIdRange($incidentCategoryId, $categoryIds) &&
             isInIdRange($incidentNidsEventId, $nidsIds) &&
             isInIdRange($incidentHidsEventId, $hidsIds) &&
             isInIdRange($incidentHoneyId, $honeypotIds)){
               // update
                $data = [
                     'name' => $incidentName,
                     'category_id' => $incidentCategoryId,
                     'date' => $incidentDate,
                     'authored_by' => $incidentAuthor,
                     'nids_event_id' => $incidentNidsEventId,
                     'hids_event_id' => $incidentHidsEventId,
                     'honeypot_event_id' => $incidentHoneyId,
                     'id_global_incident' => $incidentId
                ];
                $query = "UPDATE Incident SET name = :name,
                                             category_id = :category_id,
                                             date = :date,
                                             authored_by = :authored_by,
                                             nids_event_id = :nids_event_id,
                                             hids_event_id = :hids_event_id,
                                             honeypot_event_id = :honeypot_event_id
                         WHERE id_global_incident = :id_global_incident;";
                $send = $PDO->prepare($query);
                $send->execute($data);
                echo "<br>Incident Successfully Changed!<br>";
          }else {
                echo "Invalid input data for Incident!";
                error_log("[MySQL] UPDATE failed on change.php", 3, "/usr/share/nginx/applogs/app_events.log");
          }

    } catch (PDOException $e) {
          printDebugMsg($e->getMessage());
    }
}

function isInIdRange($value, $response) {
      $isInRange = false;
      foreach ($response as $row ) {
        if ($row[0] == $value) {
          $isInRange = true;
        }
      }
      return $isInRange;
}


function printDebugMsg($message) {
      if(PRINT_DEBUG) {
          echo "<br>[DEBUG]: $message";
      }
}
